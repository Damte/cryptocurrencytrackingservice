package com.dmgc.cryptocurrency.trackingservice.service.serviceImpl;

import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingAmountDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingWalletDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.converter.CryptocurrencyEntryConverter;
import com.dmgc.cryptocurrency.trackingservice.exception.EntryNotFound;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidAmount;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidCryptocurrency;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNotFound;
import com.dmgc.cryptocurrency.trackingservice.model.CryptocurrencyEntry;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;
import com.dmgc.cryptocurrency.trackingservice.repository.CryptocurrencyEntryRepository;
import com.dmgc.cryptocurrency.trackingservice.service.BitfinexDataService;
import com.dmgc.cryptocurrency.trackingservice.service.CryptocurrencyEntryService;
import com.dmgc.cryptocurrency.trackingservice.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CryptocurrencyEntryServiceImpl implements CryptocurrencyEntryService {

    private final CryptocurrencyEntryRepository entryRepository;
    private final CryptocurrencyEntryConverter converter;
    private final WalletService walletService;
    private final BitfinexDataService bitfinexDataService;


    @Autowired
    public CryptocurrencyEntryServiceImpl(CryptocurrencyEntryRepository entryRepository, CryptocurrencyEntryConverter converter, WalletService walletService, BitfinexDataService bitfinexDataService) {
        this.entryRepository = entryRepository;
        this.converter = converter;
        this.walletService = walletService;
        this.bitfinexDataService = bitfinexDataService;
    }

    @Override
    public Set<CryptocurrencyEntry> addNewEntry(CryptocurrencyEntryDTO dto) throws WalletNotFound, InvalidCryptocurrency, InvalidAmount, EntryNotFound {
        Wallet wallet = walletService.findWalletByName(dto.getWalletName());

        checkIfAmountForEntryCreationIsGreaterThanZero(dto.getAmountPurchased());
        bitfinexDataService.checkIfCryptocurrencyIsValid(dto.getCryptocurrencyName());
        if (checkIfEntryForThisCryptocurrencyAlreadyExists(dto.getCryptocurrencyName(), wallet.getId())) {
            updateAmountOfCryptocurrencyPurchased(wallet.getId(), dto.getCryptocurrencyName(), createNewCryptocurrencyEntryForUpdating(dto.getAmountPurchased()));
            return updateAllCryptocurrencyEntries();
        }

        Double lastPriceInEuros = bitfinexDataService.calculateLastPriceInEuros(dto.getCryptocurrencyName());
        BigDecimal currentMarketValue = calculateCurrentMarketValue(dto.getAmountPurchased(), lastPriceInEuros);
        CryptocurrencyEntry newCryptocurrencyEntry = converter.dtoToCryptocurrencyEntry(dto, currentMarketValue);


        newCryptocurrencyEntry.setWallet(wallet);
        entryRepository.save(newCryptocurrencyEntry);

        return updateAllCryptocurrencyEntries();
    }

    private void checkIfAmountForEntryCreationIsGreaterThanZero(Double amount) throws InvalidAmount {
        if (amount <= 0) {
            throw new InvalidAmount("The amount purchased must be greater than 0.");
        }
    }

    private CryptocurrencyEntryForUpdatingAmountDTO createNewCryptocurrencyEntryForUpdating(Double amountToUpdate) {
        CryptocurrencyEntryForUpdatingAmountDTO cryptocurrencyEntryForUpdatingAmountDTO = new CryptocurrencyEntryForUpdatingAmountDTO();
        cryptocurrencyEntryForUpdatingAmountDTO.setAmountPurchased(amountToUpdate);
        return cryptocurrencyEntryForUpdatingAmountDTO;
    }

    @Override
    public CryptocurrencyEntry findCryptocurrencyEntryByName(Long walletId, String cryptocurrencyName) throws EntryNotFound, WalletNotFound {
        walletService.findWalletById(walletId);
        CryptocurrencyEntry cryptocurrencyEntryInDB = entryRepository.findByCryptocurrencyNameAndWallet_Id(cryptocurrencyName, walletId).orElseThrow(() -> new EntryNotFound("There is no recorded entry with the name: " + cryptocurrencyName));

        return updateSingleCryptocurrencyEntry(cryptocurrencyEntryInDB);
    }

    @Override
    public CryptocurrencyEntry findCryptocurrencyEntryById(Long cryptocurrencyId) throws EntryNotFound {
        CryptocurrencyEntry cryptocurrencyEntryInDB = entryRepository.findById(cryptocurrencyId).orElseThrow(() -> new EntryNotFound("There is no recorded entry with the id: " + cryptocurrencyId));

        return updateSingleCryptocurrencyEntry(cryptocurrencyEntryInDB);
    }

    @Override
    public Set<CryptocurrencyEntry> updateCryptocurrencyEntriesByWallet(Long walletId) throws WalletNotFound {
        walletService.findWalletById(walletId);
        Set<CryptocurrencyEntry> cryptocurrencyEntries = entryRepository.findCryptocurrencyEntriesById(walletId);
        return updateSetOfCryptocurrencyEntries(cryptocurrencyEntries);
    }

    @Override
    public Set<CryptocurrencyEntry> updateAllCryptocurrencyEntries() {
        Set<CryptocurrencyEntry> cryptocurrencyEntries = new HashSet<>();
        entryRepository.findAll().forEach(cryptocurrencyEntries::add);

        return updateSetOfCryptocurrencyEntries(cryptocurrencyEntries);
    }

    private Set<CryptocurrencyEntry> updateSetOfCryptocurrencyEntries(Set<CryptocurrencyEntry> cryptocurrencyEntries) {
        Set<CryptocurrencyEntry> updatedCryptocurrencyEntries = cryptocurrencyEntries.stream().map(cryptocurrencyEntry -> updateSingleCryptocurrencyEntry(cryptocurrencyEntry)).collect(Collectors.toSet());

        return updatedCryptocurrencyEntries;
    }

    @Override
    public BigDecimal findCombinedValueAllEntries() {
        updateAllCryptocurrencyEntries();
        BigDecimal combinedValue = entryRepository.getSumOfCurrentMarketValues().orElse(BigDecimal.ZERO);
        return combinedValue;
    }

    @Override
    public BigDecimal findCombinedValueAllEntries(Long walletId) throws WalletNotFound {
        updateCryptocurrencyEntriesByWallet(walletId);
        BigDecimal combinedValue = entryRepository.getSumOfCurrentMarketValuesByWalletId(walletId).orElse(BigDecimal.ZERO);
        return combinedValue;
    }

    @Override
    public CryptocurrencyEntry deleteCryptocurrencyEntry(Long cryptocurrencyEntryId) throws EntryNotFound {
        CryptocurrencyEntry entryToBeDeleted = entryRepository.findById(cryptocurrencyEntryId).orElseThrow(() -> new EntryNotFound("There is no recorded entry with the id: " + cryptocurrencyEntryId));
        entryRepository.delete(entryToBeDeleted);

        return entryToBeDeleted;
    }

    @Override
    public CryptocurrencyEntry updateAmountOfCryptocurrencyPurchased(Long walletId, String cryptocurrencyEntryName, CryptocurrencyEntryForUpdatingAmountDTO cryptocurrencyEntryDTO) throws EntryNotFound, InvalidAmount {
        CryptocurrencyEntry cryptocurrencyEntryInDB = entryRepository.findByCryptocurrencyNameAndWallet_Id(cryptocurrencyEntryName, walletId).orElseThrow(() -> new EntryNotFound("There is no recorded entry with the name: " + cryptocurrencyEntryName));
        Double updatedPurchasedAmount = calculateNewAmount(cryptocurrencyEntryInDB.getAmountPurchased(), cryptocurrencyEntryDTO.getAmountPurchased());
        cryptocurrencyEntryInDB.setAmountPurchased(updatedPurchasedAmount);
        CryptocurrencyEntry updatedCryptocurrencyEntry = updateSingleCryptocurrencyEntry(cryptocurrencyEntryInDB);

        return updatedCryptocurrencyEntry;
    }


    private CryptocurrencyEntry updateSingleCryptocurrencyEntry(CryptocurrencyEntry cryptocurrencyEntryToUpdate) {
        Double lastPriceInEuros = bitfinexDataService.calculateLastPriceInEuros(cryptocurrencyEntryToUpdate.getCryptocurrencyName());
        cryptocurrencyEntryToUpdate.setCurrentValue(calculateCurrentMarketValue(cryptocurrencyEntryToUpdate.getAmountPurchased(), lastPriceInEuros));
        cryptocurrencyEntryToUpdate.setEntryUpdate(LocalDateTime.now());
        return entryRepository.save(cryptocurrencyEntryToUpdate);
    }

    private Boolean checkIfEntryForThisCryptocurrencyAlreadyExists(String cryptocurrencyName, Long walletId) {
        Long number = entryRepository.findIfExistsInThisWallet(cryptocurrencyName, walletId);
        return number > 0;
    }


    private Double calculateNewAmount(Double previousPurchasedAmount, Double newPurchasedAmount) throws InvalidAmount {
        if (newPurchasedAmount < 0 && Math.abs(newPurchasedAmount) > Math.abs(previousPurchasedAmount)) {
            throw new InvalidAmount("You can only reduce the purchasedAmount by a maximum of: " + previousPurchasedAmount);
        }
        return previousPurchasedAmount + newPurchasedAmount;
    }

    private BigDecimal calculateCurrentMarketValue(Double amount, Double lastPriceInEuros) {
        return BigDecimal.valueOf(lastPriceInEuros * amount);
    }

    @Override
    public CryptocurrencyEntry updateWalletOfCryptocurrency(Long walletId, String cryptocurrencyEntryName, CryptocurrencyEntryForUpdatingWalletDTO cryptocurrencyEntryDTO) throws EntryNotFound, WalletNotFound, InvalidAmount {
        CryptocurrencyEntry cryptocurrencyEntryInDB = entryRepository.findByCryptocurrencyNameAndWallet_Id(cryptocurrencyEntryName, walletId).orElseThrow(() -> new EntryNotFound("There is no recorded entry with the name: " + cryptocurrencyEntryName));
        Wallet destinationWallet = walletService.findWalletByName(cryptocurrencyEntryDTO.getWalletName());
        cryptocurrencyEntryInDB.setWallet(destinationWallet);
        if (checkIsThereAnEntryForThisCryptocurrency(cryptocurrencyEntryName, destinationWallet.getId()))  {
            deleteCryptocurrencyEntry(cryptocurrencyEntryInDB.getId());
            return updateAmountOfCryptocurrencyPurchased(destinationWallet.getId(), cryptocurrencyEntryName, createNewCryptocurrencyEntryForUpdating(cryptocurrencyEntryInDB.getAmountPurchased()));
        }
        CryptocurrencyEntry updatedCryptocurrencyEntry = updateSingleCryptocurrencyEntry(cryptocurrencyEntryInDB);

        return updatedCryptocurrencyEntry;
    }

    private boolean checkIsThereAnEntryForThisCryptocurrency(String cryptocurrencyName, Long walletId) {
        return entryRepository.findByCryptocurrencyNameAndWallet_Id(cryptocurrencyName, walletId).isPresent();
    }

}
