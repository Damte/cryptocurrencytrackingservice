package com.dmgc.cryptocurrency.trackingservice.service;

import com.dmgc.cryptocurrency.trackingservice.dto.WalletForCreationDTO;
import com.dmgc.cryptocurrency.trackingservice.exception.DuplicatedWallet;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidLocation;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNameAlreadyInUse;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNotFound;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;

public interface WalletService {
    Wallet findWalletByName(String name) throws WalletNotFound;

    Wallet findWalletById(Long id) throws WalletNotFound;

    Wallet createNewWallet(WalletForCreationDTO walletForCreationDTO) throws DuplicatedWallet, InvalidLocation, WalletNameAlreadyInUse;
}
