package com.dmgc.cryptocurrency.trackingservice.service.serviceImpl;

import com.dmgc.cryptocurrency.trackingservice.dto.WalletForCreationDTO;
import com.dmgc.cryptocurrency.trackingservice.exception.DuplicatedWallet;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidLocation;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNameAlreadyInUse;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNotFound;
import com.dmgc.cryptocurrency.trackingservice.model.Location;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;
import com.dmgc.cryptocurrency.trackingservice.repository.WalletRepository;
import com.dmgc.cryptocurrency.trackingservice.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    @Override
    public Wallet findWalletByName(String name) throws WalletNotFound {
        Wallet wallet = walletRepository.findByName(name).orElseThrow(() -> new WalletNotFound("There is no such a wallet, please select an already existing one."));
        return wallet;
    }

    @Override
    public Wallet findWalletById(Long id) throws WalletNotFound {
        Wallet wallet = walletRepository.findById(id).orElseThrow(() -> new WalletNotFound("There is no such a wallet, please select an already existing one."));
        return wallet;
    }

    // TODO: 06/03/2021  Add a switch case for creating Location and pass simply a String?
    @Override
    public Wallet createNewWallet(WalletForCreationDTO dto) throws DuplicatedWallet, InvalidLocation, WalletNameAlreadyInUse {
        Wallet wallet = new Wallet();
        validateWallet(dto);
        wallet.setLocation(Location.valueOf(dto.getLocation()));
        wallet.setName(dto.getName());
        Wallet savedWallet = walletRepository.save(wallet);
        return savedWallet;
    }

    private void validateWallet(WalletForCreationDTO dto) throws InvalidLocation, WalletNameAlreadyInUse {
        checkIfValidLocation(dto.getLocation());
        checkIfNameIsAlreadyInUse(dto.getName());
    }

    private void checkIfValidLocation(String location) throws InvalidLocation {
        Location[] values = Location.values();
        if (Arrays.stream(values).anyMatch(loc -> loc.name().equals(location))) {
            return;
        }
        throw new InvalidLocation(location + " is not a valid location");
    }

    private void checkIfNameIsAlreadyInUse(String name) throws WalletNameAlreadyInUse {
        if (walletRepository.existsByName(name)) {
            throw new WalletNameAlreadyInUse(name + " is already in use, please choose a different one");
        }
    }

    private void checkIfDuplicated(String location) throws DuplicatedWallet {
        if (walletRepository.existsByLocation(Location.valueOf(location))) {
            throw new DuplicatedWallet("There is already an existing wallet on this location: " + location);
        }
    }

}
