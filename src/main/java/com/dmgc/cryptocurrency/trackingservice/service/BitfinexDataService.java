package com.dmgc.cryptocurrency.trackingservice.service;

import com.dmgc.cryptocurrency.trackingservice.exception.InvalidCryptocurrency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BitfinexDataService {
    private static Map<String, String> mapOfCurrencies = new LinkedHashMap<>();
    private final RestTemplate restTemplate;
    private final String BITFINEX_API = "https://api-pub.bitfinex.com/v2/";


    @Autowired
    public BitfinexDataService(RestTemplate restTemplate) {

        this.restTemplate = restTemplate;
        findListOfCurrencies();

    }


    private void findListOfCurrencies() {
        String url = BITFINEX_API + "/conf/pub:map:currency:label";
        ResponseEntity<Object[]> responseEntity = restTemplate.getForEntity(url, Object[].class);
        Object[] objects = responseEntity.getBody();
        List<Object> list = Arrays.stream(objects).collect(Collectors.toList());
        List<Object> second = (List<Object>) list.get(0);

        second.forEach(o -> {
            ArrayList<String> stringValues = (ArrayList<String>) o;
            mapOfCurrencies.put(stringValues.get(1), stringValues.get(0));
        });
    }

    public void checkIfCryptocurrencyIsValid(String cryptocurrencyName) throws InvalidCryptocurrency {
        if (!mapOfCurrencies.containsKey(cryptocurrencyName)) {
            throw new InvalidCryptocurrency("There is not a currency with the label: " + cryptocurrencyName);
        }
    }

    private Double findLastPrice(@PathVariable("code") String cryptocurrencyCode) {
        String symbol = "t" + cryptocurrencyCode + "USD";
        String url = BITFINEX_API + "ticker/" + symbol;

        ResponseEntity<Object[]> responseEntity = restTemplate.getForEntity(url, Object[].class);
        Object[] objects = responseEntity.getBody();
        List<Object> list = Arrays.stream(objects).collect(Collectors.toList());

        String lastPriceStr = String.valueOf(list.get(6));
        Double lastPrice = Double.valueOf(lastPriceStr);
        return lastPrice;
    }

    private Double findExchangeRateDollarEuro() {
        String url = BITFINEX_API + "calc/fx";
// request body parameters
        Map<String, String> map = new HashMap<>();
        map.put("ccy1", "USD");
        map.put("ccy2", "EUR");

// send POST request
        ResponseEntity<Object[]> response = restTemplate.postForEntity(url, map, Object[].class);
        if (response.getStatusCode() == HttpStatus.OK) {
            System.out.println("Request Successful");
        } else {
            System.out.println("Request Failed");
        }
        Object[] object = response.getBody();
        Double exchangeRateDollarEuro = (Double) object[0];

        return exchangeRateDollarEuro;
    }

    public Double calculateLastPriceInEuros(String cryptocurrencyName) {
        String symbol = getCryptocurrencyCode(cryptocurrencyName);
        Double lastPriceInUSD = findLastPrice(symbol);
        Double exchangeRateFromUSDToEUR = findExchangeRateDollarEuro();
        Double lastPriceInEUR = lastPriceInUSD * exchangeRateFromUSDToEUR;
        return lastPriceInEUR;
    }

    private String getCryptocurrencyCode(String cryptocurrencyName) {
        return mapOfCurrencies.get(cryptocurrencyName);
    }
}
