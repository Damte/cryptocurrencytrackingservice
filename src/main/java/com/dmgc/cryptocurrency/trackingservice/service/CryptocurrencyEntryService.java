package com.dmgc.cryptocurrency.trackingservice.service;

import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingAmountDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingWalletDTO;
import com.dmgc.cryptocurrency.trackingservice.exception.EntryNotFound;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidAmount;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidCryptocurrency;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNotFound;
import com.dmgc.cryptocurrency.trackingservice.model.CryptocurrencyEntry;

import java.math.BigDecimal;
import java.util.Set;

public interface CryptocurrencyEntryService {
    Set<CryptocurrencyEntry> addNewEntry(CryptocurrencyEntryDTO dto) throws WalletNotFound, InvalidCryptocurrency, InvalidAmount, EntryNotFound;

    CryptocurrencyEntry findCryptocurrencyEntryByName(Long walletId, String cryptocurrencyName) throws EntryNotFound, WalletNotFound;

    CryptocurrencyEntry findCryptocurrencyEntryById(Long cryptocurrencyId) throws EntryNotFound;

    Set<CryptocurrencyEntry> updateCryptocurrencyEntriesByWallet(Long walletId) throws WalletNotFound;

    BigDecimal findCombinedValueAllEntries(Long walletId) throws WalletNotFound;

    CryptocurrencyEntry deleteCryptocurrencyEntry(Long cryptocurrencyEntryId) throws EntryNotFound;

    CryptocurrencyEntry updateAmountOfCryptocurrencyPurchased(Long walletId, String cryptocurrencyEntryName, CryptocurrencyEntryForUpdatingAmountDTO cryptocurrencyEntryDTO) throws EntryNotFound, InvalidAmount;

    CryptocurrencyEntry updateWalletOfCryptocurrency(Long previousWalletId, String cryptocurrencyEntryName, CryptocurrencyEntryForUpdatingWalletDTO cryptocurrencyEntryDTO) throws EntryNotFound, WalletNotFound, InvalidAmount;

    Set<CryptocurrencyEntry> updateAllCryptocurrencyEntries();

    BigDecimal findCombinedValueAllEntries();
}
