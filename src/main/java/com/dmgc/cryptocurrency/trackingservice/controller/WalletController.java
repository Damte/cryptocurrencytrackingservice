package com.dmgc.cryptocurrency.trackingservice.controller;

import com.dmgc.cryptocurrency.trackingservice.dto.WalletForCreationDTO;
import com.dmgc.cryptocurrency.trackingservice.exception.DuplicatedWallet;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidLocation;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNameAlreadyInUse;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;
import com.dmgc.cryptocurrency.trackingservice.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WalletController {

    private final WalletService walletService;

    @Autowired
    public WalletController(WalletService walletService) {
        this.walletService = walletService;
    }

    @PostMapping(value = "/wallet")
    public ResponseEntity<Wallet> createNewWallet(@RequestBody WalletForCreationDTO dto) {
        try {
            Wallet wallet = walletService.createNewWallet(dto);
            return new ResponseEntity<>(wallet, HttpStatus.CREATED);
        } catch (DuplicatedWallet | InvalidLocation | WalletNameAlreadyInUse exception) {
            return new ResponseEntity(exception, HttpStatus.CONFLICT);
        }
        catch (Exception  e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }
    }


}
