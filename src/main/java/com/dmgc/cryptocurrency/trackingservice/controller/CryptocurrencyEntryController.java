package com.dmgc.cryptocurrency.trackingservice.controller;

import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingAmountDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingWalletDTO;
import com.dmgc.cryptocurrency.trackingservice.exception.EntryNotFound;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidAmount;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidCryptocurrency;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNotFound;
import com.dmgc.cryptocurrency.trackingservice.model.CryptocurrencyEntry;
import com.dmgc.cryptocurrency.trackingservice.service.CryptocurrencyEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Set;

@RestController
public class CryptocurrencyEntryController {

    private final CryptocurrencyEntryService cryptocurrencyEntryService;


    @Autowired
    public CryptocurrencyEntryController(CryptocurrencyEntryService cryptocurrencyEntryService) {
        this.cryptocurrencyEntryService = cryptocurrencyEntryService;
    }


    @PostMapping(value = "/entry")
    public ResponseEntity<Set<CryptocurrencyEntry>> saveNewCryptocurrencyEntry(@RequestBody CryptocurrencyEntryDTO cryptocurrencyEntryDTO) {
        try {
            Set<CryptocurrencyEntry> cryptocurrencyEntriesIncludingSavedOne = cryptocurrencyEntryService.addNewEntry(cryptocurrencyEntryDTO);
            return new ResponseEntity<>(cryptocurrencyEntriesIncludingSavedOne, HttpStatus.CREATED);
        } catch (EntryNotFound | WalletNotFound | InvalidCryptocurrency | InvalidAmount e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }
    }


    @GetMapping(value = "wallet/{wid}/entry/{name}")
    public ResponseEntity<CryptocurrencyEntry> fetchCryptocurrencyEntryByName(@PathVariable("wid") Long walletId, @PathVariable("name") String cryptocurrencyName) {
        try {
            CryptocurrencyEntry cryptocurrencyEntry = cryptocurrencyEntryService.findCryptocurrencyEntryByName(walletId, cryptocurrencyName);
            return new ResponseEntity<>(cryptocurrencyEntry, HttpStatus.OK);
        } catch (EntryNotFound | WalletNotFound e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "entry/{id}")
    public ResponseEntity<CryptocurrencyEntry> fetchCryptocurrencyEntryById(@PathVariable("id") Long cryptocurrencyId) {
        try {
            CryptocurrencyEntry cryptocurrencyEntry = cryptocurrencyEntryService.findCryptocurrencyEntryById(cryptocurrencyId);
            return new ResponseEntity<>(cryptocurrencyEntry, HttpStatus.OK);
        } catch (EntryNotFound e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "wallet/{walletId}/entries")
    public ResponseEntity<Set<CryptocurrencyEntry>> fetchAllCryptocurrencyEntriesByWallet(@PathVariable("walletId") Long walletId) {
        try {
            Set<CryptocurrencyEntry> cryptocurrencyEntries = cryptocurrencyEntryService.updateCryptocurrencyEntriesByWallet(walletId);
            return new ResponseEntity<>(cryptocurrencyEntries, HttpStatus.OK);
        } catch (WalletNotFound walletNotFound) {
            return new ResponseEntity(walletNotFound, HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping(value = "wallet/{walletId}/entries/combined-value")
    public ResponseEntity<BigDecimal> findCombinedValueOfAllEntriesByWallet(@PathVariable("walletId") Long walletId) {
        try {
            BigDecimal combinedValue = cryptocurrencyEntryService.findCombinedValueAllEntries(walletId);
            return new ResponseEntity<>(combinedValue, HttpStatus.OK);
        } catch (WalletNotFound walletNotFound) {
            return new ResponseEntity(walletNotFound, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "entries")
    public ResponseEntity<Set<CryptocurrencyEntry>> fetchAllCryptocurrenciesEntries() {
        Set<CryptocurrencyEntry> cryptocurrencyEntries = cryptocurrencyEntryService.updateAllCryptocurrencyEntries();
        return new ResponseEntity<>(cryptocurrencyEntries, HttpStatus.OK);

    }

    @GetMapping(value = "entries/combined-value")
    public ResponseEntity<BigDecimal> findCombinedValueOfAllEntries() {
        BigDecimal combinedValue = cryptocurrencyEntryService.findCombinedValueAllEntries();
        return new ResponseEntity<>(combinedValue, HttpStatus.OK);
    }

    @DeleteMapping(value = "/entry/{id}")
    public ResponseEntity<CryptocurrencyEntry> deleteCryptocurrencyEntry(@PathVariable("id") Long cryptocurrencyEntryId) {
        try {
            CryptocurrencyEntry cryptocurrencyEntry = cryptocurrencyEntryService.deleteCryptocurrencyEntry(cryptocurrencyEntryId);
            return new ResponseEntity<>(cryptocurrencyEntry, HttpStatus.OK);
        } catch (EntryNotFound e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }
    }

    @PatchMapping(value = "wallet/{walletId}/entry/{name}/amount")
    public ResponseEntity<CryptocurrencyEntry> updateCryptocurrencyEntryAmount(@PathVariable("walletId") Long walletId, @PathVariable("name") String cryptocurrencyEntryName, @RequestBody CryptocurrencyEntryForUpdatingAmountDTO cryptocurrencyEntryDTO) {
        try {
            CryptocurrencyEntry updatedCryptocurrencyEntry = cryptocurrencyEntryService.updateAmountOfCryptocurrencyPurchased(walletId, cryptocurrencyEntryName, cryptocurrencyEntryDTO);
            return new ResponseEntity<>(updatedCryptocurrencyEntry, HttpStatus.OK);
        } catch (EntryNotFound | InvalidAmount e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }
    }

    @PatchMapping(value = "wallet/{walletId}/entry/{name}/location")
    public ResponseEntity<CryptocurrencyEntry> updateWalletOfCryptocurrencyEntry(@PathVariable("walletId") Long walletId, @PathVariable("name") String cryptocurrencyEntryName, @RequestBody CryptocurrencyEntryForUpdatingWalletDTO cryptocurrencyEntryDTO) {
        try {
            CryptocurrencyEntry updatedCryptocurrencyEntry = cryptocurrencyEntryService.updateWalletOfCryptocurrency(walletId, cryptocurrencyEntryName, cryptocurrencyEntryDTO);
            return new ResponseEntity<>(updatedCryptocurrencyEntry, HttpStatus.OK);
        } catch (EntryNotFound | WalletNotFound e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }

    }
}
