package com.dmgc.cryptocurrency.trackingservice.repository;

import com.dmgc.cryptocurrency.trackingservice.model.CryptocurrencyEntry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

@Repository
public interface CryptocurrencyEntryRepository extends CrudRepository<CryptocurrencyEntry, Long> {
    @Query("FROM CryptocurrencyEntry AS c WHERE c.wallet.id=:id")
    Set<CryptocurrencyEntry> findCryptocurrencyEntriesById(@Param("id") Long walletId);

    Optional<CryptocurrencyEntry> findByCryptocurrencyNameAndWallet_Id(String cryptocurrencyEntryName, Long walletId);

    @Query("SELECT COUNT(c.id) from CryptocurrencyEntry AS c WHERE c.wallet.id=:id AND c.cryptocurrencyName=:name")
    Long findIfExistsInThisWallet(@Param("name") String cryptocurrencyName, @Param("id") Long walletId);

    @Query("SELECT SUM(c.currentValue) FROM CryptocurrencyEntry AS c")
    Optional<BigDecimal> getSumOfCurrentMarketValues();

    @Query("SELECT SUM(c.currentValue) FROM CryptocurrencyEntry AS c WHERE c.wallet.id=:id")
    Optional<BigDecimal> getSumOfCurrentMarketValuesByWalletId(@Param("id") Long walletId);
}
