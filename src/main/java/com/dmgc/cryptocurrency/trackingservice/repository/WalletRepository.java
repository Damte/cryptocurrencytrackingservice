package com.dmgc.cryptocurrency.trackingservice.repository;

import com.dmgc.cryptocurrency.trackingservice.model.Location;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WalletRepository extends CrudRepository<Wallet, Long> {
    Optional<Wallet> findByLocation(Location location);

    Optional<Wallet> findByName(String name);

    Boolean existsByLocation(Location location);

    Boolean existsByName(String name);
}
