package com.dmgc.cryptocurrency.trackingservice.exception;

public class InvalidLocation extends Exception {
    public InvalidLocation(String s) {
        super(s);
    }

}
