package com.dmgc.cryptocurrency.trackingservice.exception;

public class DuplicatedWallet extends Exception {
    public DuplicatedWallet(String s) {
        super(s);
    }

}
