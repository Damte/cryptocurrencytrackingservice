package com.dmgc.cryptocurrency.trackingservice.exception;

public class InvalidCryptocurrency extends Exception {
    public InvalidCryptocurrency(String s) {
        super(s);
    }

}
