package com.dmgc.cryptocurrency.trackingservice.exception;

public class WalletNameAlreadyInUse extends Exception {
    public WalletNameAlreadyInUse(String s) {
        super(s);
    }

}
