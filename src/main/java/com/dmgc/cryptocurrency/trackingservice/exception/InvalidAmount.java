package com.dmgc.cryptocurrency.trackingservice.exception;

public class InvalidAmount extends Exception {
    public InvalidAmount(String s) {
        super(s);
    }

}
