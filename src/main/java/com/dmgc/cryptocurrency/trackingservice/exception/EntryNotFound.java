package com.dmgc.cryptocurrency.trackingservice.exception;

public class EntryNotFound extends Exception {
    public EntryNotFound(String s) {
        super(s);
    }

}
