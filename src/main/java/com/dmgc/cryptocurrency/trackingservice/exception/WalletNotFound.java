package com.dmgc.cryptocurrency.trackingservice.exception;

public class WalletNotFound extends Exception {
    public WalletNotFound(String s) {
        super(s);
    }

}
