package com.dmgc.cryptocurrency.trackingservice.dto;

public class CryptocurrencyEntryForUpdatingWalletDTO {

    private String walletName;

    public String getWalletName() {
        return walletName;
    }

    public void setWalletName(String walletName) {
        this.walletName = walletName;
    }
}
