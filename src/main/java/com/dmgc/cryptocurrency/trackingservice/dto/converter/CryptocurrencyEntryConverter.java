package com.dmgc.cryptocurrency.trackingservice.dto.converter;

import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryDTO;
import com.dmgc.cryptocurrency.trackingservice.model.CryptocurrencyEntry;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component
public class CryptocurrencyEntryConverter {

    public CryptocurrencyEntry dtoToCryptocurrencyEntry(CryptocurrencyEntryDTO dto, BigDecimal currentMarketValue) {
        CryptocurrencyEntry cryptocurrencyEntry = new CryptocurrencyEntry();

        cryptocurrencyEntry.setCryptocurrencyName(dto.getCryptocurrencyName());
        cryptocurrencyEntry.setAmountPurchased(dto.getAmountPurchased());
        cryptocurrencyEntry.setEntryCreation(LocalDateTime.now());
        cryptocurrencyEntry.setEntryUpdate(LocalDateTime.now());

        cryptocurrencyEntry.setCurrentValue(currentMarketValue);

        return cryptocurrencyEntry;
    }


}
