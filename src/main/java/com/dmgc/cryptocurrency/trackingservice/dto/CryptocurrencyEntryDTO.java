package com.dmgc.cryptocurrency.trackingservice.dto;

public class CryptocurrencyEntryDTO {

    private String cryptocurrencyName;
    private Double amountPurchased;
    private String walletName;

    public String getCryptocurrencyName() {
        return cryptocurrencyName;
    }

    public void setCryptocurrencyName(String cryptocurrencyName) {
        this.cryptocurrencyName = cryptocurrencyName;
    }

    public Double getAmountPurchased() {
        return amountPurchased;
    }

    public void setAmountPurchased(Double amountPurchased) {
        this.amountPurchased = amountPurchased;
    }

    public String getWalletName() {
        return walletName;
    }

    public void setWalletName(String walletName) {
        this.walletName = walletName;
    }
}
