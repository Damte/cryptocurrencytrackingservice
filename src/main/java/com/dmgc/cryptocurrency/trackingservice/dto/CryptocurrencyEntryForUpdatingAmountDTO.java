package com.dmgc.cryptocurrency.trackingservice.dto;

public class CryptocurrencyEntryForUpdatingAmountDTO {

    private Double amountPurchased;

    public Double getAmountPurchased() {
        return amountPurchased;
    }

    public void setAmountPurchased(Double amountPurchased) {
        this.amountPurchased = amountPurchased;
    }
}
