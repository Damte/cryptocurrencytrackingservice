package com.dmgc.cryptocurrency.trackingservice.model;

public enum Location {
        WEB_WALLET, DESKTOP_WALLET, MOBILE_WALLET, HARDWARE_WALLET, PAPER_WALLET
}
