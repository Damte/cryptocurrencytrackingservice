package com.dmgc.cryptocurrency.trackingservice.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class CryptocurrencyEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String cryptocurrencyName;

    private Double amountPurchased;

    private LocalDateTime entryCreation;
    private LocalDateTime entryUpdate;

    @ManyToOne
    @JoinColumn(name = "wallet_id", nullable = false)
//    @JsonIgnore
    private Wallet wallet;

    private BigDecimal currentValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCryptocurrencyName() {
        return cryptocurrencyName;
    }

    public void setCryptocurrencyName(String cryptocurrencyName) {
        this.cryptocurrencyName = cryptocurrencyName;
    }

    public Double getAmountPurchased() {
        return amountPurchased;
    }

    public void setAmountPurchased(Double amountPurchased) {
        this.amountPurchased = amountPurchased;
    }

    public LocalDateTime getEntryCreation() {
        return entryCreation;
    }

    public void setEntryCreation(LocalDateTime entryCreation) {
        this.entryCreation = entryCreation;
    }

    public LocalDateTime getEntryUpdate() {
        return entryUpdate;
    }

    public void setEntryUpdate(LocalDateTime entryUpdate) {
        this.entryUpdate = entryUpdate;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public BigDecimal getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(BigDecimal currentValue) {
        this.currentValue = currentValue;
    }
}
