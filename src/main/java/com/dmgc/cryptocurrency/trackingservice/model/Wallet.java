package com.dmgc.cryptocurrency.trackingservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Location location;

    @Column(unique=true, nullable=false)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "wallet")
    @JsonIgnore
    private List<CryptocurrencyEntry> cryptocurrencies;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CryptocurrencyEntry> getCryptocurrencies() {
        return cryptocurrencies;
    }

    public void setCryptocurrencies(List<CryptocurrencyEntry> cryptocurrencies) {
        this.cryptocurrencies = cryptocurrencies;
    }
}
