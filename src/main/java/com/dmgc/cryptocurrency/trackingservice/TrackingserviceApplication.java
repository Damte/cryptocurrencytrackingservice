package com.dmgc.cryptocurrency.trackingservice;

import com.dmgc.cryptocurrency.trackingservice.model.CryptocurrencyEntry;
import com.dmgc.cryptocurrency.trackingservice.model.Location;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;
import com.dmgc.cryptocurrency.trackingservice.repository.CryptocurrencyEntryRepository;
import com.dmgc.cryptocurrency.trackingservice.repository.WalletRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SpringBootApplication
@EnableSwagger2
public class TrackingserviceApplication implements CommandLineRunner {

    private final CryptocurrencyEntryRepository entryRepository;
    private final WalletRepository walletRepository;

    public TrackingserviceApplication(CryptocurrencyEntryRepository entryRepository, WalletRepository walletRepository) {
        this.entryRepository = entryRepository;
        this.walletRepository = walletRepository;
    }


    public static void main(String[] args) {
        SpringApplication.run(TrackingserviceApplication.class, args);
    }

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.dmgc.cryptocurrency")).build();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
