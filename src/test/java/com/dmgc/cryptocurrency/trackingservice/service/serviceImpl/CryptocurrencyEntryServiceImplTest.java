package com.dmgc.cryptocurrency.trackingservice.service.serviceImpl;


import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingAmountDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingWalletDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.converter.CryptocurrencyEntryConverter;
import com.dmgc.cryptocurrency.trackingservice.exception.EntryNotFound;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidAmount;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidCryptocurrency;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNotFound;
import com.dmgc.cryptocurrency.trackingservice.model.CryptocurrencyEntry;
import com.dmgc.cryptocurrency.trackingservice.model.Location;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;
import com.dmgc.cryptocurrency.trackingservice.repository.CryptocurrencyEntryRepository;
import com.dmgc.cryptocurrency.trackingservice.service.BitfinexDataService;
import com.dmgc.cryptocurrency.trackingservice.service.WalletService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CryptocurrencyEntryServiceImplTest {
    @Mock
    CryptocurrencyEntryRepository repository;
    @Mock
    CryptocurrencyEntryConverter converter;
    @Mock
    WalletService walletService;
    @Mock
    BitfinexDataService bitfinexDataService;

    @InjectMocks
    CryptocurrencyEntryServiceImpl entryService;
    CryptocurrencyEntryDTO cryptocurrencyEntryDTO = new CryptocurrencyEntryDTO();
    CryptocurrencyEntryForUpdatingAmountDTO cryptocurrencyEntryForUpdatingAmountDTO = new CryptocurrencyEntryForUpdatingAmountDTO();
    CryptocurrencyEntryForUpdatingWalletDTO cryptocurrencyEntryForUpdatingWalletDTO = new CryptocurrencyEntryForUpdatingWalletDTO();
    private CryptocurrencyEntry cryptocurrencyEntry = new CryptocurrencyEntry();
    private Wallet wallet = new Wallet();

    @BeforeEach
    public void init() {

        wallet.setLocation(Location.HARDWARE_WALLET);
        wallet.setId(1l);
        cryptocurrencyEntry.setEntryCreation(LocalDateTime.now());
        cryptocurrencyEntry.setEntryUpdate(LocalDateTime.now());
        cryptocurrencyEntry.setId(1l);
        cryptocurrencyEntry.setAmountPurchased(10.0);
        cryptocurrencyEntry.setCurrentValue(BigDecimal.valueOf(100));
        cryptocurrencyEntry.setCryptocurrencyName("Bitcoin");
        cryptocurrencyEntry.setWallet(wallet);
        wallet.setCryptocurrencies(List.of(cryptocurrencyEntry));
    }

    @Test
    public void testAddNewEntrySuccess() throws WalletNotFound, InvalidCryptocurrency, InvalidAmount, EntryNotFound {
        Mockito.when(walletService.findWalletByName(Mockito.anyString())).thenReturn(wallet);
        Mockito.doNothing().when(bitfinexDataService).checkIfCryptocurrencyIsValid(Mockito.anyString());

        cryptocurrencyEntryDTO.setAmountPurchased(10.05);
        cryptocurrencyEntryDTO.setCryptocurrencyName("Bitcoin");
        cryptocurrencyEntryDTO.setWalletName("Bitfinex wallet");
        Mockito.when(repository.findIfExistsInThisWallet(Mockito.anyString(), Mockito.anyLong())).thenReturn(0l);
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(converter.dtoToCryptocurrencyEntry(Mockito.any(CryptocurrencyEntryDTO.class), Mockito.any(BigDecimal.class))).thenReturn(cryptocurrencyEntry);

        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);
        Mockito.when(repository.findAll()).thenReturn(Set.of(cryptocurrencyEntry));
        Set<CryptocurrencyEntry> allCryptocurrencyEntries = entryService.addNewEntry(cryptocurrencyEntryDTO);

        Assertions.assertTrue(allCryptocurrencyEntries.contains(cryptocurrencyEntry));
        Assertions.assertTrue(allCryptocurrencyEntries.size() > 0);
        Mockito.verify(bitfinexDataService).checkIfCryptocurrencyIsValid("Bitcoin");
        Mockito.verify(repository, Mockito.atLeast(2)).save(cryptocurrencyEntry);
    }

    @Test
    public void testAddNewEntryFailsByAmountBeingZero() throws WalletNotFound, InvalidCryptocurrency, InvalidAmount, EntryNotFound {
        cryptocurrencyEntryDTO.setAmountPurchased(Double.valueOf(0));
        cryptocurrencyEntryDTO.setCryptocurrencyName("Bitcoin");
        cryptocurrencyEntryDTO.setWalletName("Bitfinex wallet");

        Exception exception = Assertions.assertThrows(InvalidAmount.class, () -> {
            Set<CryptocurrencyEntry> allCryptocurrencyEntries = entryService.addNewEntry(cryptocurrencyEntryDTO);
        });
        Assertions.assertEquals("The amount purchased must be greater than 0.", exception.getMessage());
        Mockito.verify(walletService).findWalletByName(cryptocurrencyEntryDTO.getWalletName());
        Mockito.verifyNoInteractions(bitfinexDataService);
        Mockito.verifyNoInteractions(repository);


    }

    @Test
    public void testAddNewEntryFailsByAmountBeingNegative() throws WalletNotFound, InvalidCryptocurrency, InvalidAmount, EntryNotFound {
        cryptocurrencyEntryDTO.setAmountPurchased(Double.valueOf(-55));
        cryptocurrencyEntryDTO.setCryptocurrencyName("Bitcoin");
        cryptocurrencyEntryDTO.setWalletName("Bitfinex wallet");

        Exception exception = Assertions.assertThrows(InvalidAmount.class, () -> {
            Set<CryptocurrencyEntry> allCryptocurrencyEntries = entryService.addNewEntry(cryptocurrencyEntryDTO);
        });
        Assertions.assertEquals("The amount purchased must be greater than 0.", exception.getMessage());
        Mockito.verify(walletService).findWalletByName(cryptocurrencyEntryDTO.getWalletName());
        Mockito.verifyNoInteractions(bitfinexDataService);
        Mockito.verifyNoInteractions(repository);

    }

    @Test
    public void testFindCryptocurrencyEntryByNameSuccess() throws EntryNotFound, WalletNotFound {

        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.findByCryptocurrencyNameAndWallet_Id(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.ofNullable(cryptocurrencyEntry));
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        CryptocurrencyEntry cryptocurrencyEntry = entryService.findCryptocurrencyEntryByName(1l, "Bitcoin");

        Assertions.assertEquals(1l, cryptocurrencyEntry.getId());
        Assertions.assertEquals("Bitcoin", cryptocurrencyEntry.getCryptocurrencyName());
    }

    @Test
    public void testFindCryptocurrencyEntryByNameThrowsExceptionIfDatabaseReturnsNull() throws EntryNotFound, WalletNotFound {

        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.findByCryptocurrencyNameAndWallet_Id(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        String cryptocurrencyName = "Bitcoin";
        Exception exception = Assertions.assertThrows(EntryNotFound.class, () -> {
            CryptocurrencyEntry cryptocurrencyEntry = entryService.findCryptocurrencyEntryByName(1l, cryptocurrencyName);
        });
        Assertions.assertEquals("There is no recorded entry with the name: " + cryptocurrencyName, exception.getMessage());
    }

    @Test
    public void testFindCryptocurrencyEntryByIdSuccess() throws EntryNotFound {

        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(cryptocurrencyEntry));
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        CryptocurrencyEntry cryptocurrencyEntry = entryService.findCryptocurrencyEntryById(1l);

        Assertions.assertEquals(1l, cryptocurrencyEntry.getId());
        Assertions.assertEquals("Bitcoin", cryptocurrencyEntry.getCryptocurrencyName());
    }

    @Test
    public void testFindCryptocurrencyEntryByIdThrowsExceptionIfDatabaseReturnsNull() {

        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        Long cryptocurrencyId = 5l;
        Exception exception = Assertions.assertThrows(EntryNotFound.class, () -> {
            CryptocurrencyEntry cryptocurrencyEntry = entryService.findCryptocurrencyEntryById(cryptocurrencyId);
        });
        Assertions.assertEquals("There is no recorded entry with the id: " + cryptocurrencyId, exception.getMessage());
        Mockito.verifyNoInteractions(bitfinexDataService);
    }

    @Test
    public void testUpdateCryptocurrencyEntriesByWalletSuccess() throws WalletNotFound {
        Mockito.when(walletService.findWalletById(Mockito.anyLong())).thenReturn(wallet);
        Mockito.when(repository.findCryptocurrencyEntriesById(Mockito.anyLong())).thenReturn(Set.of(cryptocurrencyEntry));
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        Set<CryptocurrencyEntry> savedCryptocurrencyEntries = entryService.updateCryptocurrencyEntriesByWallet(1l);

        Assertions.assertTrue(savedCryptocurrencyEntries.size() > 0);
        Assertions.assertTrue(savedCryptocurrencyEntries.contains(cryptocurrencyEntry));
    }

    @Test
    public void testUpdateCryptocurrencyEntriesByWalletSuccessIfThereAreNoEntriesInDatabase() throws WalletNotFound {
        Mockito.when(walletService.findWalletById(Mockito.anyLong())).thenReturn(wallet);
        Mockito.when(repository.findCryptocurrencyEntriesById(Mockito.anyLong())).thenReturn(Set.of());
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        Set<CryptocurrencyEntry> savedCryptocurrencyEntries = entryService.updateCryptocurrencyEntriesByWallet(1l);

        Assertions.assertTrue(savedCryptocurrencyEntries.size() == 0);
        Assertions.assertNotNull(savedCryptocurrencyEntries);
    }

    @Test
    public void testUpdateCryptocurrencyEntriesSuccess() {
        Iterable<CryptocurrencyEntry> cryptocurrencyEntryIterable = List.of(cryptocurrencyEntry);
        Mockito.when(repository.findAll()).thenReturn(cryptocurrencyEntryIterable);
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        Set<CryptocurrencyEntry> savedCryptocurrencyEntries = entryService.updateAllCryptocurrencyEntries();

        Assertions.assertTrue(savedCryptocurrencyEntries.size() > 0);
        Assertions.assertTrue(savedCryptocurrencyEntries.contains(cryptocurrencyEntry));
    }

    @Test
    public void testUpdateCryptocurrencyEntriesSuccessIfThereAreNoEntriesInDatabase() {
        Iterable<CryptocurrencyEntry> cryptocurrencyEntryIterable = List.of();
        Mockito.when(repository.findAll()).thenReturn(cryptocurrencyEntryIterable);
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        Set<CryptocurrencyEntry> savedCryptocurrencyEntries = entryService.updateAllCryptocurrencyEntries();

        Assertions.assertTrue(savedCryptocurrencyEntries.size() == 0);
        Assertions.assertNotNull(savedCryptocurrencyEntries);
    }

    @Test
    public void testFindCombinedValueAllEntriesSuccess() {
        Iterable<CryptocurrencyEntry> cryptocurrencyEntryIterable = List.of(cryptocurrencyEntry);
        Mockito.when(repository.findAll()).thenReturn(cryptocurrencyEntryIterable);
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);
        Mockito.when(repository.getSumOfCurrentMarketValues()).thenReturn(Optional.ofNullable(BigDecimal.valueOf(750)));

        BigDecimal combinedValue = entryService.findCombinedValueAllEntries();

        Assertions.assertTrue(combinedValue.compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertNotNull(combinedValue);
    }

    @Test
    public void testFindCombinedValueAllEntriesSuccessIfPortfolioIsStillEmpty() {
        Iterable<CryptocurrencyEntry> cryptocurrencyEntryIterable = List.of(cryptocurrencyEntry);
        Mockito.when(repository.findAll()).thenReturn(cryptocurrencyEntryIterable);
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);
        Mockito.when(repository.getSumOfCurrentMarketValues()).thenReturn(Optional.ofNullable(null));

        BigDecimal combinedValue = entryService.findCombinedValueAllEntries();

        Assertions.assertTrue(combinedValue.compareTo(BigDecimal.ZERO) == 0);
        Assertions.assertNotNull(combinedValue);
    }

    @Test
    public void testFindCombinedValueAllEntriesByWalletSuccess() throws WalletNotFound {
        Iterable<CryptocurrencyEntry> cryptocurrencyEntryIterable = List.of(cryptocurrencyEntry);
        Mockito.when(repository.findAll()).thenReturn(cryptocurrencyEntryIterable);
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);
        Mockito.when(repository.getSumOfCurrentMarketValuesByWalletId(Mockito.anyLong())).thenReturn(Optional.ofNullable(BigDecimal.valueOf(750)));

        BigDecimal combinedValue = entryService.findCombinedValueAllEntries(1l);

        Assertions.assertTrue(combinedValue.compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertNotNull(combinedValue);
    }

    @Test
    public void testFindCombinedValueAllEntriesByWalletSuccessIfPortfolioIsStillEmpty() throws WalletNotFound {
        Iterable<CryptocurrencyEntry> cryptocurrencyEntryIterable = List.of(cryptocurrencyEntry);
        Mockito.when(repository.findAll()).thenReturn(cryptocurrencyEntryIterable);
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);
        Mockito.when(repository.getSumOfCurrentMarketValuesByWalletId(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));

        BigDecimal combinedValue = entryService.findCombinedValueAllEntries(1l);

        Assertions.assertTrue(combinedValue.compareTo(BigDecimal.ZERO) == 0);
        Assertions.assertNotNull(combinedValue);
    }

    @Test
    public void testDeleteCryptocurrencyEntrySuccess() throws EntryNotFound {
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(cryptocurrencyEntry));
        Mockito.doNothing().when(repository).delete(Mockito.any(CryptocurrencyEntry.class));

        CryptocurrencyEntry deletedValue = entryService.deleteCryptocurrencyEntry(1l);

        Assertions.assertEquals(1l, deletedValue.getId());
        Assertions.assertEquals("Bitcoin", deletedValue.getCryptocurrencyName());
        Assertions.assertNotNull(deletedValue);
    }

    @Test
    public void testDeleteCryptocurrencyEntryThrowExceptionIfThereIsNoEntryWithThatId() {
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));

        Long cryptocurrencyId = 5l;

        Exception exception = Assertions.assertThrows(EntryNotFound.class, () -> {
            CryptocurrencyEntry deletedValue = entryService.deleteCryptocurrencyEntry(cryptocurrencyId);
        });
        Assertions.assertEquals("There is no recorded entry with the id: " + cryptocurrencyId, exception.getMessage());
        Mockito.verify(repository, Mockito.never()).delete(Mockito.any(CryptocurrencyEntry.class));

    }

    @Test
    public void testUpdateAmountOfCryptocurrencyPurchasedSuccess() throws EntryNotFound, InvalidAmount {
        Mockito.when(repository.findByCryptocurrencyNameAndWallet_Id(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.ofNullable(cryptocurrencyEntry));
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        cryptocurrencyEntryForUpdatingAmountDTO.setAmountPurchased(Double.valueOf(-10));

        CryptocurrencyEntry updatedCryptocurrencyEntry = entryService.updateAmountOfCryptocurrencyPurchased(1l, "Bitcoin", cryptocurrencyEntryForUpdatingAmountDTO);

        Assertions.assertEquals(1l, updatedCryptocurrencyEntry.getId());
        Assertions.assertEquals("Bitcoin", updatedCryptocurrencyEntry.getCryptocurrencyName());
        Assertions.assertNotNull(updatedCryptocurrencyEntry);
        Mockito.verify(repository).save(cryptocurrencyEntry);
    }

    @Test
    public void testUpdateAmountOfCryptocurrencyPurchasedThrowsExceptionIfThereIsNotThatEntryOnWallet() throws EntryNotFound {
        Mockito.when(repository.findByCryptocurrencyNameAndWallet_Id(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        cryptocurrencyEntryForUpdatingAmountDTO.setAmountPurchased(Double.valueOf(-55));


        String cryptocurrencyName = "Bitcoin";

        Exception exception = Assertions.assertThrows(EntryNotFound.class, () -> {
            CryptocurrencyEntry updatedCryptocurrencyEntry = entryService.updateAmountOfCryptocurrencyPurchased(1l, cryptocurrencyName, cryptocurrencyEntryForUpdatingAmountDTO);
        });
        Assertions.assertEquals("There is no recorded entry with the name: " + cryptocurrencyName, exception.getMessage());
        Mockito.verifyNoInteractions(bitfinexDataService);
        Mockito.verify(repository, Mockito.never()).save(Mockito.any(CryptocurrencyEntry.class));
    }

    @Test
    public void testUpdateAmountOfCryptocurrencyPurchasedThrowsExceptionWhenAmountIsReducedToLessThanZero() throws EntryNotFound {
        Mockito.when(repository.findByCryptocurrencyNameAndWallet_Id(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.ofNullable(cryptocurrencyEntry));
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        cryptocurrencyEntryForUpdatingAmountDTO.setAmountPurchased(Double.valueOf(-105));


        String cryptocurrencyName = "Bitcoin";

        Exception exception = Assertions.assertThrows(InvalidAmount.class, () -> {
            CryptocurrencyEntry updatedCryptocurrencyEntry = entryService.updateAmountOfCryptocurrencyPurchased(1l, cryptocurrencyName, cryptocurrencyEntryForUpdatingAmountDTO);
        });
        Assertions.assertEquals("You can only reduce the purchasedAmount by a maximum of: " + cryptocurrencyEntry.getAmountPurchased(), exception.getMessage());
        Mockito.verifyNoInteractions(bitfinexDataService);
        Mockito.verify(repository, Mockito.never()).save(Mockito.any(CryptocurrencyEntry.class));
    }
    @Test
    public void testUpdateWalletOfCryptocurrencySuccess() throws EntryNotFound, InvalidAmount, WalletNotFound {
        Mockito.when(repository.findByCryptocurrencyNameAndWallet_Id(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.ofNullable(cryptocurrencyEntry)).thenReturn(Optional.ofNullable(null));
        Mockito.when(walletService.findWalletByName(Mockito.anyString())).thenReturn(wallet);
        Mockito.when(bitfinexDataService.calculateLastPriceInEuros(Mockito.anyString())).thenReturn(100.05);
        Mockito.when(repository.save(Mockito.any(CryptocurrencyEntry.class))).thenReturn(cryptocurrencyEntry);

        cryptocurrencyEntryForUpdatingWalletDTO.setWalletName("Hardware wallet 02");

        CryptocurrencyEntry updatedCryptocurrencyEntry = entryService.updateWalletOfCryptocurrency(1l, "Bitcoin", cryptocurrencyEntryForUpdatingWalletDTO);

        Assertions.assertEquals(1l, updatedCryptocurrencyEntry.getId());
        Assertions.assertEquals("Bitcoin", updatedCryptocurrencyEntry.getCryptocurrencyName());
        Assertions.assertNotNull(updatedCryptocurrencyEntry);
        Mockito.verify(repository).save(cryptocurrencyEntry);
    }
    @Test
    public void testUpdateWalletOfCryptocurrencyThrowsEntryNotFoundException() throws WalletNotFound {
        Mockito.when(repository.findByCryptocurrencyNameAndWallet_Id(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.ofNullable(null));

        cryptocurrencyEntryForUpdatingWalletDTO.setWalletName("Hardware wallet 02");

        String cryptocurrencyName = "Bitcoin";

        Exception exception = Assertions.assertThrows(EntryNotFound.class, () -> {
            CryptocurrencyEntry updatedCryptocurrencyEntry = entryService.updateWalletOfCryptocurrency(1l, cryptocurrencyName, cryptocurrencyEntryForUpdatingWalletDTO);
        });
        Assertions.assertEquals("There is no recorded entry with the name: " + cryptocurrencyName, exception.getMessage());
        Mockito.verifyNoInteractions(bitfinexDataService);
        Mockito.verifyNoInteractions(walletService);
        Mockito.verify(repository, Mockito.never()).save(Mockito.any(CryptocurrencyEntry.class));

    }

}
