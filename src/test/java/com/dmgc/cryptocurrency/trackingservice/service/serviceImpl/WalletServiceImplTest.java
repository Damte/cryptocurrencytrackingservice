package com.dmgc.cryptocurrency.trackingservice.service.serviceImpl;

import com.dmgc.cryptocurrency.trackingservice.dto.WalletForCreationDTO;
import com.dmgc.cryptocurrency.trackingservice.exception.DuplicatedWallet;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidLocation;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNameAlreadyInUse;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNotFound;
import com.dmgc.cryptocurrency.trackingservice.model.Location;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;
import com.dmgc.cryptocurrency.trackingservice.repository.WalletRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class WalletServiceImplTest {

    @Mock
    private WalletRepository walletRepository;

    @InjectMocks
    private WalletServiceImpl walletService;

    private Wallet wallet = new Wallet();
    private WalletForCreationDTO dto = new WalletForCreationDTO();


    @BeforeAll
    public static void init() {
        System.out.println("BeforeAll init() method called");

    }

    @BeforeEach
    public void initEach() {
        System.out.println("Before Each initEach() method called");
        wallet.setId(1l);
        wallet.setLocation(Location.DESKTOP_WALLET);
        wallet.setName("Web wallet 01");
        when(walletRepository.save(any(Wallet.class))).thenReturn(wallet);
        when(walletRepository.existsByName(anyString())).thenReturn(false).thenReturn(true);

    }


    @Test
    public void testCreateNewWalletSuccess() throws InvalidLocation, DuplicatedWallet, WalletNameAlreadyInUse {
        String location = "DESKTOP_WALLET";
        dto.setLocation(location);
        String name = "desktop_01";
        dto.setName(name);

        Wallet savedWallet = walletService.createNewWallet(dto);

        Assertions.assertTrue(savedWallet.getId().equals(1l));
        Assertions.assertEquals("DESKTOP_WALLET", savedWallet.getLocation().name());
    }

    @Test
    public void testCreateNewWalletFailureRandomString() {
        String location = "something";
        dto.setLocation(location);
        String name = "desktop_01";
        dto.setName(name);

        Exception exception = Assertions.assertThrows(InvalidLocation.class, () -> walletService.createNewWallet(dto));
        Assertions.assertEquals(location + " is not a valid location", exception.getMessage());

    }

    @Test
    public void testCreateNewWalletFailsWithDuplicatedName() throws InvalidLocation, DuplicatedWallet, WalletNameAlreadyInUse {
        String location = "DESKTOP_WALLET";
        dto.setLocation(location);
        String name = "desktop_01";
        dto.setName(name);

        Wallet firstSaved = walletService.createNewWallet(dto);
        Exception exception = Assertions.assertThrows(WalletNameAlreadyInUse.class, () -> walletService.createNewWallet(dto));
        Assertions.assertEquals(name + " is already in use, please choose a different one", exception.getMessage());

    }

    @Test
    public void testFindWalletByNameIfSuccess() throws WalletNotFound {
        String name = "Web wallet 01";
        when(walletRepository.findByName(anyString())).thenReturn(Optional.ofNullable(wallet));
        Wallet savedWallet = walletService.findWalletByName(name);
        Assertions.assertTrue(savedWallet.getId().equals(1l));
        Assertions.assertEquals("Web wallet 01", savedWallet.getName());
        Assertions.assertNotNull(savedWallet.getLocation());
        Assertions.assertNotNull(savedWallet);
    }

    @Test
    public void testFindWalletByLocationIfGettingNull() {
        String name = "Web wallet 01";
        when(walletRepository.findByName(anyString())).thenReturn(Optional.ofNullable(null));
        assertThrows(WalletNotFound.class, () -> walletService.findWalletByName(name));
    }
}
