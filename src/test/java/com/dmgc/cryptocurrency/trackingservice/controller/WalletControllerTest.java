package com.dmgc.cryptocurrency.trackingservice.controller;

import com.dmgc.cryptocurrency.trackingservice.dto.WalletForCreationDTO;
import com.dmgc.cryptocurrency.trackingservice.exception.DuplicatedWallet;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidLocation;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNameAlreadyInUse;
import com.dmgc.cryptocurrency.trackingservice.model.CryptocurrencyEntry;
import com.dmgc.cryptocurrency.trackingservice.model.Location;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;
import com.dmgc.cryptocurrency.trackingservice.service.WalletService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class WalletControllerTest {

    @Mock
    private WalletService walletService;

    @InjectMocks
    private WalletController walletController;

    private Wallet wallet = new Wallet();
    private WalletForCreationDTO dto = new WalletForCreationDTO();
    private CryptocurrencyEntry cryptocurrencyEntry = new CryptocurrencyEntry();


    @BeforeEach
    void setWallet() {
        wallet.setLocation(Location.DESKTOP_WALLET);
        wallet.setCryptocurrencies(List.of(cryptocurrencyEntry));
        wallet.setId(1l);

    }

    @BeforeEach
    void setMockOutput() throws DuplicatedWallet, InvalidLocation, WalletNameAlreadyInUse {
        when(walletService.createNewWallet(Mockito.any(WalletForCreationDTO.class))).thenReturn(wallet).thenThrow(new WalletNameAlreadyInUse("Duplicated!"));
    }

    @AfterEach
    void cleanUp() {
        if (wallet.getId() != null) {
            wallet = new Wallet();
            dto = new WalletForCreationDTO();
        }
    }

    @Test
    public void testCreateNewWalletSuccess() {
        String location = "DESKTOP_WALLET";
        String name = "desktop_01";

        dto.setName(name);
        dto.setLocation(location);

        ResponseEntity response = walletController.createNewWallet(dto);
        Wallet responseWallet = (Wallet) response.getBody();
        Assertions.assertEquals(201, response.getStatusCode().value());
        Assertions.assertTrue(response.hasBody());
        Assertions.assertTrue(responseWallet.getCryptocurrencies().size() > 0);
    }

    @Test
    public void testCreateNewWalletIfThrowsExceptionNameDuplicated() {
        ResponseEntity response1 = walletController.createNewWallet(dto);
        ResponseEntity response2 = walletController.createNewWallet(dto);
        Assertions.assertEquals(409, response2.getStatusCode().value());
        Assertions.assertTrue(response2.toString().contains("Duplicated!"));
    }


}
