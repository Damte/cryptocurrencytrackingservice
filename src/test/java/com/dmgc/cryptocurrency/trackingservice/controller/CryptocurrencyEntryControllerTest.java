package com.dmgc.cryptocurrency.trackingservice.controller;

import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingAmountDTO;
import com.dmgc.cryptocurrency.trackingservice.dto.CryptocurrencyEntryForUpdatingWalletDTO;
import com.dmgc.cryptocurrency.trackingservice.exception.EntryNotFound;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidAmount;
import com.dmgc.cryptocurrency.trackingservice.exception.InvalidCryptocurrency;
import com.dmgc.cryptocurrency.trackingservice.exception.WalletNotFound;
import com.dmgc.cryptocurrency.trackingservice.model.CryptocurrencyEntry;
import com.dmgc.cryptocurrency.trackingservice.model.Wallet;
import com.dmgc.cryptocurrency.trackingservice.service.CryptocurrencyEntryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CryptocurrencyEntryControllerTest {

    @Mock
    CryptocurrencyEntryService service;

    @InjectMocks
    CryptocurrencyEntryController controller;

    private CryptocurrencyEntry cryptocurrencyEntry = new CryptocurrencyEntry();

    @BeforeEach
    public void init() {

        System.out.println("BeforeEach init() method called");
        cryptocurrencyEntry.setEntryCreation(LocalDateTime.now());
        cryptocurrencyEntry.setEntryUpdate(LocalDateTime.now());
        cryptocurrencyEntry.setId(1l);
        cryptocurrencyEntry.setAmountPurchased(10.0);
        cryptocurrencyEntry.setCurrentValue(BigDecimal.valueOf(100));
        cryptocurrencyEntry.setCryptocurrencyName("Bitcoin");
        cryptocurrencyEntry.setWallet(new Wallet());
    }

    @Test
    void testSaveNewCryptocurrencyEntrySuccess() throws EntryNotFound, InvalidAmount, WalletNotFound, InvalidCryptocurrency {
            Mockito.when(service.addNewEntry(any(CryptocurrencyEntryDTO.class))).thenReturn(Set.of(cryptocurrencyEntry));


        ResponseEntity<Set<CryptocurrencyEntry>> entryResponse = controller.saveNewCryptocurrencyEntry(new CryptocurrencyEntryDTO());

        Assertions.assertNotNull(entryResponse);
        Assertions.assertEquals(201, entryResponse.getStatusCodeValue());
        Assertions.assertTrue( entryResponse.getBody().contains(cryptocurrencyEntry));
    }

    @Test
    void testSaveNewCryptocurrencyEntryThrowsException() throws EntryNotFound, InvalidAmount, WalletNotFound, InvalidCryptocurrency {
        Mockito.when(service.addNewEntry(Mockito.any(CryptocurrencyEntryDTO.class))).thenThrow(new EntryNotFound("Entry not found"));
        ResponseEntity<Set<CryptocurrencyEntry>> response = controller.saveNewCryptocurrencyEntry(new CryptocurrencyEntryDTO());

        Assertions.assertEquals(409, response.getStatusCode().value());
    }
    @Test
    void testSaveNewCryptocurrencyEntryThrowsException2() throws EntryNotFound, InvalidAmount, WalletNotFound, InvalidCryptocurrency {
        Mockito.when(service.addNewEntry(Mockito.any(CryptocurrencyEntryDTO.class))).thenThrow(new WalletNotFound("Wallet not found"));
        ResponseEntity<Set<CryptocurrencyEntry>> response = controller.saveNewCryptocurrencyEntry(new CryptocurrencyEntryDTO());

        Assertions.assertEquals(409, response.getStatusCode().value());
    }
    @Test
    void testSaveNewCryptocurrencyEntryThrowsException3() throws EntryNotFound, InvalidAmount, WalletNotFound, InvalidCryptocurrency {
        Mockito.when(service.addNewEntry(Mockito.any(CryptocurrencyEntryDTO.class))).thenThrow(new InvalidCryptocurrency("Invalid cryptocurrency"));
        ResponseEntity<Set<CryptocurrencyEntry>> response = controller.saveNewCryptocurrencyEntry(new CryptocurrencyEntryDTO());

        Assertions.assertEquals(409, response.getStatusCode().value());
    }
    @Test
    void testSaveNewCryptocurrencyEntryThrowsException4() throws EntryNotFound, InvalidAmount, WalletNotFound, InvalidCryptocurrency {
        Mockito.when(service.addNewEntry(Mockito.any(CryptocurrencyEntryDTO.class))).thenThrow(new InvalidAmount("Invalid amount"));
        ResponseEntity<Set<CryptocurrencyEntry>> response = controller.saveNewCryptocurrencyEntry(new CryptocurrencyEntryDTO());

        Assertions.assertEquals(409, response.getStatusCode().value());
    }

    @Test
    void testFetchCryptocurrencyEntryByNameSuccess() throws EntryNotFound, WalletNotFound {
        Mockito.when(service.findCryptocurrencyEntryByName(Mockito.anyLong(), Mockito.anyString())).thenReturn(cryptocurrencyEntry);
        ResponseEntity<CryptocurrencyEntry> entryResponse = controller.fetchCryptocurrencyEntryByName(1l, "Bitcoin");

        Assertions.assertNotNull(entryResponse.getBody());
        Assertions.assertEquals(200, entryResponse.getStatusCodeValue());
        Assertions.assertEquals(1l, entryResponse.getBody().getId());
    }

    @Test
    void testFetchCryptocurrencyEntryByNameThrowsException() throws EntryNotFound, WalletNotFound {
        Mockito.when(service.findCryptocurrencyEntryByName(Mockito.anyLong(), Mockito.anyString())).thenThrow(new EntryNotFound("Entry not found"));
        ResponseEntity<CryptocurrencyEntry> entryResponse = controller.fetchCryptocurrencyEntryByName(1l, "Random text");

        Assertions.assertEquals(409, entryResponse.getStatusCodeValue());
        Assertions.assertTrue(entryResponse.toString().contains("Entry not found"));
    }
    @Test
    void testFetchCryptocurrencyEntryByNameThrowsException2() throws EntryNotFound, WalletNotFound {
        Mockito.when(service.findCryptocurrencyEntryByName(Mockito.anyLong(), Mockito.anyString())).thenThrow(new WalletNotFound("Wallet not found"));
        ResponseEntity<CryptocurrencyEntry> entryResponse = controller.fetchCryptocurrencyEntryByName(1l, "Bitcoin");

        Assertions.assertEquals(409, entryResponse.getStatusCodeValue());
        Assertions.assertTrue(entryResponse.toString().contains("Wallet not found"));
    }

    @Test
    void testFetchCryptocurrencyEntryByIdSuccess() throws EntryNotFound {
        Mockito.when(service.findCryptocurrencyEntryById(Mockito.anyLong())).thenReturn(cryptocurrencyEntry);
        ResponseEntity<CryptocurrencyEntry> entryResponse = controller.fetchCryptocurrencyEntryById(1l);

        Assertions.assertNotNull(entryResponse.getBody());
        Assertions.assertEquals(200, entryResponse.getStatusCodeValue());
        Assertions.assertEquals(1l, entryResponse.getBody().getId());
    }
    @Test
    void testFetchCryptocurrencyEntryByIdThrowsException() throws EntryNotFound {
        Mockito.when(service.findCryptocurrencyEntryById(Mockito.anyLong())).thenThrow(new EntryNotFound("Entry not found"));
        ResponseEntity<CryptocurrencyEntry> entryResponse = controller.fetchCryptocurrencyEntryById(1l);

        Assertions.assertEquals(409, entryResponse.getStatusCodeValue());
    }

    @Test
    void testFetchAllCryptocurrencyEntriesByWalletSuccess() throws WalletNotFound {
        Mockito.when(service.updateCryptocurrencyEntriesByWallet(Mockito.anyLong())).thenReturn(Set.of(cryptocurrencyEntry));
        ResponseEntity<Set<CryptocurrencyEntry>> entryResponse = controller.fetchAllCryptocurrencyEntriesByWallet(1l);
        Set<CryptocurrencyEntry> cryptocurrencyEntrySet = entryResponse.getBody();

        Assertions.assertNotNull(entryResponse.getBody());
        Assertions.assertEquals(200, entryResponse.getStatusCodeValue());
        Assertions.assertTrue(cryptocurrencyEntrySet.size()>0);
        Assertions.assertTrue(cryptocurrencyEntrySet.contains(cryptocurrencyEntry));
    }

    @Test
    void testFetchAllCryptocurrencyEntriesByWalletThrowsException() throws WalletNotFound {
        Mockito.when(service.updateCryptocurrencyEntriesByWallet(Mockito.anyLong())).thenThrow(new WalletNotFound("Wallet not found"));
        ResponseEntity<Set<CryptocurrencyEntry>> entryResponse = controller.fetchAllCryptocurrencyEntriesByWallet(1l);

        Assertions.assertEquals(404, entryResponse.getStatusCodeValue());
        Assertions.assertTrue(entryResponse.toString().contains("Wallet not found"));

    }

    @Test
    void testFindCombinedValueOfAllEntriesByWalletSuccess() throws WalletNotFound {
        Mockito.when(service.findCombinedValueAllEntries(Mockito.anyLong())).thenReturn(BigDecimal.valueOf(100));
        ResponseEntity<BigDecimal> entryResponse = controller.findCombinedValueOfAllEntriesByWallet(1l);

        Assertions.assertNotNull(entryResponse.getBody());
        Assertions.assertEquals(200, entryResponse.getStatusCodeValue());
        Assertions.assertTrue(entryResponse.getBody().compareTo(BigDecimal.ZERO)>0);
        Assertions.assertEquals(BigDecimal.valueOf(100).stripTrailingZeros(), entryResponse.getBody().stripTrailingZeros());
    }

    @Test
    void testFindCombinedValueOfAllEntriesByWalletThrowsException() throws WalletNotFound {
        Mockito.when(service.findCombinedValueAllEntries(Mockito.anyLong())).thenThrow(new WalletNotFound("Wallet not found"));
        ResponseEntity<BigDecimal> entryResponse = controller.findCombinedValueOfAllEntriesByWallet(1l);

        Assertions.assertEquals(404, entryResponse.getStatusCodeValue());
        Assertions.assertTrue(entryResponse.toString().contains("Wallet not found"));
    }

    @Test
    void testFetchAllCryptocurrencyEntriesSuccess()  {
        Mockito.when(service.updateAllCryptocurrencyEntries()).thenReturn(Set.of(cryptocurrencyEntry));
        ResponseEntity<Set<CryptocurrencyEntry>> entryResponse = controller.fetchAllCryptocurrenciesEntries();

        Assertions.assertNotNull(entryResponse.getBody());
        Assertions.assertEquals(200, entryResponse.getStatusCodeValue());
        Assertions.assertTrue(entryResponse.getBody().size()>0);
        Assertions.assertTrue(entryResponse.getBody().contains(cryptocurrencyEntry));
    }
    @Test
    void testFindCombinedValueOfAllEntriesSuccess()  {
        Mockito.when(service.findCombinedValueAllEntries()).thenReturn(BigDecimal.valueOf(100));
        ResponseEntity<BigDecimal> entryResponse = controller.findCombinedValueOfAllEntries();

        Assertions.assertNotNull(entryResponse.getBody());
        Assertions.assertEquals(200, entryResponse.getStatusCodeValue());
        Assertions.assertTrue(entryResponse.getBody().compareTo(BigDecimal.ZERO)>0);
        Assertions.assertEquals(BigDecimal.valueOf(100).stripTrailingZeros(), entryResponse.getBody().stripTrailingZeros());
    }
    @Test
    void testDeleteCryptocurrencyEntrySuccess() throws EntryNotFound {
        Mockito.when(service.deleteCryptocurrencyEntry(Mockito.anyLong())).thenReturn(cryptocurrencyEntry);
        ResponseEntity<CryptocurrencyEntry> deletedEntryResponse = controller.deleteCryptocurrencyEntry(1l);

        Assertions.assertNotNull(deletedEntryResponse.getBody());
        Assertions.assertEquals(200, deletedEntryResponse.getStatusCodeValue());
        Assertions.assertEquals(1l, deletedEntryResponse.getBody().getId());
    }
    @Test
    void testDeleteCryptocurrencyEntryThrowsException() throws EntryNotFound {
        Mockito.when(service.deleteCryptocurrencyEntry(Mockito.anyLong())).thenThrow(new EntryNotFound("Entry not found"));
        ResponseEntity<CryptocurrencyEntry> deletedEntryResponse = controller.deleteCryptocurrencyEntry(1l);
        String exceptionString = deletedEntryResponse.toString();
        Assertions.assertEquals(409, deletedEntryResponse.getStatusCodeValue());
        Assertions.assertTrue(exceptionString.contains("Entry not found"));
    }
    @Test
    void testUpdateCryptocurrencyEntrySuccess() throws EntryNotFound, InvalidAmount {
        Mockito.when(service.updateAmountOfCryptocurrencyPurchased(Mockito.anyLong(), Mockito.anyString(), Mockito.any(CryptocurrencyEntryForUpdatingAmountDTO.class))).thenReturn(cryptocurrencyEntry);
        ResponseEntity<CryptocurrencyEntry> updatedEntryResponse = controller.updateCryptocurrencyEntryAmount(1l, "Bitcoin", new CryptocurrencyEntryForUpdatingAmountDTO());

        Assertions.assertNotNull(updatedEntryResponse.getBody());
        Assertions.assertEquals(200, updatedEntryResponse.getStatusCodeValue());
        Assertions.assertEquals(1l, updatedEntryResponse.getBody().getId());
    }
    @Test
    void testUpdateCryptocurrencyEntryThrowsException() throws EntryNotFound, InvalidAmount {
        Mockito.when(service.updateAmountOfCryptocurrencyPurchased(Mockito.anyLong(), Mockito.anyString(), Mockito.any(CryptocurrencyEntryForUpdatingAmountDTO.class))).thenThrow(new InvalidAmount("Entry not found"));
        ResponseEntity<CryptocurrencyEntry> updatedEntryResponse = controller.updateCryptocurrencyEntryAmount(1l, "Bitcoin", new CryptocurrencyEntryForUpdatingAmountDTO());

        Assertions.assertEquals(409, updatedEntryResponse.getStatusCodeValue());
        Assertions.assertTrue(updatedEntryResponse.toString().contains("Entry not found"));
    }
    @Test
    void testUpdateCryptocurrencyEntryThrowsException2() throws EntryNotFound, InvalidAmount {
        Mockito.when(service.updateAmountOfCryptocurrencyPurchased(Mockito.anyLong(), Mockito.anyString(), Mockito.any(CryptocurrencyEntryForUpdatingAmountDTO.class))).thenThrow(new InvalidAmount("Not a valid amount"));
        ResponseEntity<CryptocurrencyEntry> updatedEntryResponse = controller.updateCryptocurrencyEntryAmount(1l, "Bitcoin", new CryptocurrencyEntryForUpdatingAmountDTO());

        Assertions.assertEquals(409, updatedEntryResponse.getStatusCodeValue());
        Assertions.assertTrue(updatedEntryResponse.toString().contains("Not a valid amount"));
    }
    @Test
    void testUpdateWalletOfCryptocurrencyEntrySuccess() throws EntryNotFound, WalletNotFound, InvalidAmount {
        Mockito.when(service.updateWalletOfCryptocurrency(Mockito.anyLong(), Mockito.anyString(), Mockito.any(CryptocurrencyEntryForUpdatingWalletDTO.class))).thenReturn(cryptocurrencyEntry);
        ResponseEntity<CryptocurrencyEntry> updatedEntryResponse = controller.updateWalletOfCryptocurrencyEntry(1l, "Bitcoin", new CryptocurrencyEntryForUpdatingWalletDTO());

        Assertions.assertNotNull(updatedEntryResponse.getBody());
        Assertions.assertEquals(200, updatedEntryResponse.getStatusCodeValue());
        Assertions.assertEquals(1l, updatedEntryResponse.getBody().getId());
    }
    @Test
    void testUpdateWalletOfCryptocurrencyEntryThrowsException() throws EntryNotFound, WalletNotFound, InvalidAmount {
        Mockito.when(service.updateWalletOfCryptocurrency(Mockito.anyLong(), Mockito.anyString(), Mockito.any(CryptocurrencyEntryForUpdatingWalletDTO.class))).thenThrow(new EntryNotFound("Entry not found"));
        ResponseEntity<CryptocurrencyEntry> updatedEntryResponse = controller.updateWalletOfCryptocurrencyEntry(1l, "Bitcoin", new CryptocurrencyEntryForUpdatingWalletDTO());

        Assertions.assertEquals(409, updatedEntryResponse.getStatusCodeValue());
        Assertions.assertTrue(updatedEntryResponse.toString().contains("Entry not found"));
    }
    @Test
    void testUpdateWalletOfCryptocurrencyEntryThrowsException2() throws EntryNotFound, WalletNotFound, InvalidAmount {
        Mockito.when(service.updateWalletOfCryptocurrency(Mockito.anyLong(), Mockito.anyString(), Mockito.any(CryptocurrencyEntryForUpdatingWalletDTO.class))).thenThrow(new WalletNotFound("Wallet not found"));
        ResponseEntity<CryptocurrencyEntry> updatedEntryResponse = controller.updateWalletOfCryptocurrencyEntry(1l, "Bitcoin", new CryptocurrencyEntryForUpdatingWalletDTO());

        Assertions.assertEquals(409, updatedEntryResponse.getStatusCodeValue());
        Assertions.assertTrue(updatedEntryResponse.toString().contains("Wallet not found"));
    }

}
