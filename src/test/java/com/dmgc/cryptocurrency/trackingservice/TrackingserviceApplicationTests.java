package com.dmgc.cryptocurrency.trackingservice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class TrackingserviceApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void shouldShowSimpleAssertion() {
		Assertions.assertEquals(1, 1);
	}

}
