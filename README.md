# Cryptocurrency portfolio tracking service


Currently, the data is stored on a MySQL relational database, with the name: cryptocurrency-portfolio-tracking-service. The username and password for local access are set on src/main/resources/application.properties.
Once the project is running, then the following actions can be performed.

* First of all, it is required to set a wallet to keep track of our cryptocurrencies (POST http://localhost:8091/wallet)
    {
      "location": "HARDWARE_WALLET",
      "name": "My first wallet"
    }
 I've considered other options, but finally I decided on Wallet entity with a unique name, and an Enum of location types. I'm assuming that being for single use, the hypothetical user would only have a rather fixed number of locations for its wallets, but still I wanted to give the flexibility of identifying them with a string, in case the user would have more than one web wallet, for example.
* After there is already a wallet in the system. It is possible to add some record of user's cryptocurrencies. (POST http://localhost:8091/entry)
    {
      "amountPurchased": 10,
      "cryptocurrencyName": "Ethereum",
      "walletName": "My first wallet"
    }
There is some validation on all the property values, so for example if you would want to add some cryptocurrency that is not included on the bitfinex system, it would return an exception.
I don't like at the moment that you need to write the precise word "Ethereum" to do the record, I would like that the user could also write the code "ETH" and maybe even to do it on ignore case, but at the moment I have set it like that because it matched best the requirements.
Same goes with the response, at the moment, whenever we add a new entry, we get an updated list of all entries. I considered if it would be more efficient to only return all entries when the user asked specifically for them (I imagine if we have 200 entries maybe is best to just return the created entry and not having to update all of them every time). I have kept to what the task asked, but maybe is something to consider were this a real app.
Also, I've thought that in the hypothetical case that the user wouldn't remember if he had a record of that specific cryptocurrency on that specific wallet and were he to create a new record of an existing one, then simply this method would update the value.
* The user can receive an update of the entries he has individually by id (GET http://localhost:8091/entry/{id}), by name (GET http://localhost:8091/wallet/{wallet-id}/entry/{cryptocurrency-name}) or collectively by wallet (GET http://localhost:8091/wallet/{wallet-id}/entries) or regardless of wallet (GET http://localhost:8091/entries) Each of those would retrieve the updated current market value at the time of the query.
* I have considered two possible cases for modifying an existing entry.
    - If the amount purchased changes (increase/decrease) (PATCH http://localhost:8091/wallet/{wallet-id}/entry/{cryptocurrency-name}/amount) 
        {
          "amountPurchased": 10
        }
    - If the user wishes to change his cryptocurrency to a different wallet (PATCH http://localhost:8091/wallet/{wallet-id}/entry/{cryptocurrency-name}/new-wallet)
        {
          "walletName": "Second wallet"
        }
* The user can also find out the combined market value of all his added cryptocurrencies, either by wallet (GET http://localhost:8091/wallet/{wallet-id}/entries/combined-value) or globally (GET http://localhost:8091/entries/combined-value).
* Finally, he could entirely delete an entry from the system by doing (DELETE http://localhost:8091/entry/{id})
